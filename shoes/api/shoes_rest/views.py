from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import Shoe, BinVO


class BinEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "id",
        "import_href",
        "closet_name",
        "bin_number",
        "bin_size",
    ]
class ShoeEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "id",
        "manufacturer_name",
        "model_name",
        "color",
        "photo_url",
        "bin",
    ]
    encoders = {
        "bin": BinEncoder(),
    }

class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "id",
        "manufacturer_name",
        "model_name",
        "color",
        "photo_url",
        "bin",
    ]
    encoders = {
        "bin": BinEncoder(),
    }


@require_http_methods(["GET","POST"])
def api_shoes(request):
    """
    GET:
    Returns a dictionary with a single key "
    """
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes":shoes},
            encoder=ShoeEncoder,
        )
    elif request.method == "POST":
        content = json.loads(request.body)

        # Get the Bin object and put it into the content dict
        try:
            bin_href = content["bin"]["import_href"]
            bin = BinVO.objects.get(import_href=bin_href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message":"This bin does not exist"}
            )

        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )

@require_http_methods(["DELETE","GET","PUT"])
def api_shoe(request,pk):
    """
    Single-object API for the shoe resource.
    """
    if request.method == "GET":
        try:
            shoe = Shoe.objects.get(id=pk)
            return JsonResponse(
                shoe,
                encoder=ShoeDetailEncoder,
                safe=False
            )
        except Shoe.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        count, _ = Shoe.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})


@require_http_methods(["GET"])
def api_binVO(request):
    """
    List view to check if binVOs are polling properly.
    """
    if request.method == "GET":
        bin = BinVO.objects.all()
        return JsonResponse(
            {"bins": bin},
            encoder=BinEncoder,
            safe=False
        )
