from django.contrib import admin
from .models import BinVO, Shoe

@admin.register(Shoe)
class ShoeAdmin(admin.ModelAdmin):
    list_display = [
        'manufacturer_name',
        'model_name',
        'color',
        'photo_url',
        'bin',
    ]
