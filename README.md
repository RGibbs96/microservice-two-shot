# Wardrobify

Team:

* Person 1 - Ryan
* Person 2 - Uzair

## Design

## Shoes microservice

Need to create two models within the Shoes microservice, the actual shoe (entity) and a Bin Value Object. The shoe will have fields for the manufacturer name, model name, color, photo url, and will have a bin field which is a foreign key to the BinVO model. The BinVO model will have only the important defining information taken from the actually Bin model defined in the Wardrobe microservice, as well as the unique href generated in the wardrobe microservice. Will include the closet name, bin number, and bin size, although bin size will not be necessary for the scope of the project as far as front end display goes. I will create views to list the shoes, and relevant bin information, so that this data can be accessed via React.

The poller will periodically make calls to the wardrobe api and update / create new BinVOs based on the database that exists associated to the Wardobe Microservice.


## Hats microservice

The LocationVO model represents a physical location in the wardrobe. It has fields for the closet name, section number, and shelf number. Additionally, there is a field for import_href, which is a unique identifier for the location in the wardrobe.

The Hat model represents a hat and has fields for the fabric, style_name, color and picture_url. It also has a foreign key to the LocationVO model, which represents the physical location of the hat in the wardrobe. This allows the Hats microservice to integrate with the wardrobe microservice, by linking the hat to its physical location in the wardrobe.

This way, when the user wants to view all the hats, they can see where the hat is located in the wardrobe. They can also add new hats by specifying the location in the wardrobe. This way, the Wardrobe microservice keeps track of all the clothes and their location, and the Hats microservice keeps track of the hats and their location in the wardrobe.
