import React from 'react'
import { NavLink} from "react-router-dom"





function HatsList(props) {
    const deleteHat = async (id) => {
        const response = await fetch (`http://localhost:8090/api/hats/${id}/`, {
            method: "delete",
        })
        if (response.ok) {
            props.getHats()
        }
    }
    if (props.hats === undefined) {
        return null
    }



    return (
      <div>
        <NavLink className="nav-link" to="/hats/new">Add a new hat</NavLink>

        <table className="table table-striped table-hover align-middle mt-5">
          <thead>
            <tr>
              <th>Hat Fabic</th>
              <th>Hat Style Name</th>
              <th>Hat Color</th>
              <th>Location</th>
              <th>Photo</th>
              <th>Delete hat</th>


            </tr>
          </thead>
          <tbody>
            {props.hats.map(hat => {
                return (
                    <tr key={hat.id}>
                        <td>{ hat.fabric }</td>
                        <td>{ hat.style_name }</td>
                        <td>{ hat.color }</td>
                        <td>{ hat.location }</td>
                        <td><img src={ hat.picture_url } className="img-thumbnail hats" width="300"></img></td>
                        <td>
                            <button type="button" value={hat.id} onClick={() => deleteHat(hat.id)}>Delete</button>
                        </td>
                    </tr>
                );
            })}
          </tbody>
        </table>
      </div>

    )
}

export default HatsList;
