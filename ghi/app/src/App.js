import {useEffect, useState} from "react"
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatsList from './components/HatsList';
import HatForm from './components/HatForm';
import ShoeList from './ShoeList';
import ShoeForm from "./ShoeForm";





function App(props) {
  const [shoes, setShoes] = useState([])
  const [hats, setHats] = useState([])


  const fetchShoes = async () => {
    const url = "http://localhost:8080/api/shoes/"
    const response = await fetch(url)

    if (response.ok) {
      const data = await response.json()
      setShoes(data.shoes)
    }
  }

  const getHats = async () => {
    const url = 'http://localhost:8090/api/hats/'
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      const hats = data.hats
      setHats(hats)
    }
  }

  useEffect(() => {
    fetchShoes();
    getHats();
  }, [])


  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>

          <Route path="/" element={<MainPage />} />

          <Route path="shoes">
              <Route index element={<ShoeList shoes={shoes} fetchShoes={fetchShoes} />} />
              <Route path='new' element={<ShoeForm fetchShoes={fetchShoes} />} />
            </Route>

            <Route path="hats">
              <Route index element={<HatsList hats={hats} getHats={getHats} />} />
              <Route path='new' element={<HatForm getHats={getHats} />} />
            </Route>




        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;


// function App(props) {
  // const [hats, setHats] = useState("")
  // const fetchHats = async () => {
  //   const url = "http://localhost:8090/api/hats/"
  //   const response = await fetch(url)

  //   if (response.ok) {
  //     const data = await response.json()
  //     setHats(data.hats)
  //   }
  // }



//   useEffect(()=> {
//     fetchHats();
//     fetchShoes();
//   }, [])

//   return (
//     <BrowserRouter>
//       <Nav />
//       <div className="container">
//         <Routes>
//           <Route path="/" element={<MainPage />} />
//           <Route path="hats" element={<HatsList hats={hats} fetchHats={fetchHats}/>} />
//           <Route path="hats/new" element={<HatForm fetchHats={fetchHats} />} />
//           <Route path="shoes" element={<ShoeList shoes={shoes} fetchShoes={fetchShoes}/>} />
//           <Route path="shoes/new" element={<ShoeForm fetchShoes={fetchShoes} />} />
//         </Routes>
//       </div>
//     </BrowserRouter>
//   );
// }

// export default App;
